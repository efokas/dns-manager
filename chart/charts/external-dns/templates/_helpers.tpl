{{/* vim: set filetype=mustache: */}}

{{/*
Return the proper External DNS image name
*/}}
{{- define "external-dns.image" -}}
{{- $registryName := .Values.image.registry -}}
{{- $repositoryName := .Values.image.repository -}}
{{- $tag := .Values.image.tag | toString -}}
{{- printf "%s/%s:%s" $registryName $repositoryName $tag -}}
{{- end -}}

{{- define "external-dns.app-name" -}}
{{- printf "%s-%s" .Values.name .Values.viewName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.owner-id" -}}
{{- printf "%s-%s-%s" .Values.environmentName .Values.name .Values.viewName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.static-entry" -}}
{{- printf "%s-%s-%s" "static-entries" .Values.name .Values.viewName | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.group" -}}
{{- printf "%s.%s" .Values.name "cern.ch" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "external-dns.labels-filter" -}}
{{- $domain := include "external-dns.group" . -}}
{{- printf "dns-manager.webservices.cern.ch/domain=%s,dns-manager.webservices.cern.ch/view=%s" $domain .Values.viewName -}}
{{- end -}}
